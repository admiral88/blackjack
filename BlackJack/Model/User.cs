﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    class User
    {
        public string Name { get; set; }
        public int Coins { get; set; } 
        public int Bet { get; set; }
        public List<int> Cards;
        public bool IsLose { get; set; }

        public User()
        {
            Cards = new List<int>();
        }
        
    }


}
