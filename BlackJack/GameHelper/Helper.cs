﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJack.Model;
namespace BlackJack.Views
{
  static class Helper
  {
    public static void Show(List<int> list)
    {
      foreach (int card in list)
      {
        if (card == 2)
        {
          Console.WriteLine(Picture.CardPicture.Jack);
        }
        if (card == 3)
        {
          Console.WriteLine(Picture.CardPicture.Queen);
        }
        if (card == 4)
        {
          Console.WriteLine(Picture.CardPicture.King);
        }
        if (card == 11)
        {
          Console.WriteLine(Picture.CardPicture.Ace);
        }

        if (card != 2 && card != 3 && card != 4 && card != 11)
        {
          Console.WriteLine(card);
        }
      }
    }

    public static void Score(List<User> users, User dealer)
    {
      foreach (User user in users)
      {
        Console.Write(user.Name + " - " + user.Cards.Sum() + " | ");
      }
      Console.WriteLine(dealer.Name + " - " + dealer.Cards.Sum() + "\n");
    }
  }
}
