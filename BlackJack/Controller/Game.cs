﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJack.Views;

namespace BlackJack
{
  class Game
  {
    private List<User> _users = new List<User>();
    private Random _r = new Random();
    User Dealer = new User() { Name = "DEALER", IsLose = false };
    int numberPlayers;

    public void Execute()
    {
      Console.WriteLine("START? Y/N");
      char start = Console.ReadKey().KeyChar;
      Console.WriteLine("\r");
      if (!start.ToString().StartsWith("y") && !start.ToString().StartsWith("Y"))
      {
        Console.WriteLine("Game over");
        return;
      }
      Console.Clear();
      Console.WriteLine("input number of players");
      string stringNumberPlayers;
      do
      {
        stringNumberPlayers = Console.ReadLine();
      }
      while (!int.TryParse(stringNumberPlayers, out numberPlayers));

      for (int i = 0; i < numberPlayers; i++)
      {
        _users.Add(new User() { Name = "USER " + (i + 1) });
      }
      for (int i = 0; i < _users.Count; i++)
      {
        int tmp;
        var IsParse = false;
        do
        {
          Console.WriteLine(_users[i].Name + " input your Money");
          IsParse = int.TryParse(Console.ReadLine(), out tmp);
          if (IsParse)
          {
            _users[i].Coins = tmp;
          }
        }
        while (!IsParse);
      }
      Start();
    }

    public void Start()
    {
      CheckUsers();
      MakeBet();
      DistributionCards();
      GameUsers();
      Helper.Score(_users, Dealer);
      Finish();
    }


    public void CheckUsers()
    {
      Console.WriteLine();
      for (int i = _users.Count - 1; i >= 0; i--)
      {
        _users[i].Bet = 0;
        if (_users[i].Coins <= 0)
        {
          Console.WriteLine("User - {0} finished the game", _users[i].Name);
          _users.RemoveAt(i);
        }
      }
      if (_users.Count == 0)
      {
        Console.WriteLine("Game over");
        Environment.Exit(0);
      }
    }


    public void MakeBet()
    {
      foreach (User item in _users)
      {
        Console.WriteLine(item.Name + " COINS = " + item.Coins);
      }
      Console.WriteLine();
      Dealer.Cards.Clear();
      Dealer.IsLose = false;
      for (int i = 0; i < _users.Count; i++)
      {
        string stringBet = "";
        int intParse;
        _users[i].Cards.Clear();
        do
        {
          Console.WriteLine("{0} coins = {1}\nMake your bet", _users[i].Name, _users[i].Coins);
          stringBet = Console.ReadLine();
          if (int.TryParse(stringBet, out intParse))
          {
            _users[i].Bet = intParse;
          }
          if (_users[i].Bet > _users[i].Coins || _users[i].Bet <= 0)
          {
            Console.Clear();
            Console.WriteLine("error input");
          }
        } while (_users[i].Bet > _users[i].Coins || _users[i].Bet <= 0);
      }
    }


    public void DistributionCards()
    {
      Console.Clear();
      foreach (User user in _users)
      {
        GiveOneCard(user);
        GiveOneCard(user);
      }
      GiveOneCard(Dealer);
      GiveOneCard(Dealer);
    }


    public void GameUsers()
    {
      for (int i = 0; i < _users.Count; i++)
      {
        char keychar;
        do
        {
          //Console.Clear();
          Helper.Score(_users, Dealer);
          Console.WriteLine(_users[i].Name + " Sum Cards = " + _users[i].Cards.Sum() + "\n");
          Helper.Show(_users[i].Cards);
          Console.WriteLine();
          Console.WriteLine("Get a card? Y\nEnd the game? F\nNext User N");
          keychar = Console.ReadKey().KeyChar;
          if (keychar == 'Y' || keychar == 'y')
          {
            GiveOneCard(_users[i]);
          }
          if (keychar == 'f' || keychar == 'F')
          {
            Console.Clear();
            Finish();           
          }
          if (keychar == 'n' || keychar == 'N')
          {
            Console.Clear();
          }
        } while (keychar != 'f' && keychar != 'n');
        Console.WriteLine();
      }
    }


    public void Finish()
    {
      while (Dealer.Cards.Sum() <= 16)
      {
        GiveOneCard(Dealer);
        Helper.Score(_users, Dealer);
      }
      if (Dealer.Cards.Sum() > 21)
      {
        Console.WriteLine("Dealer lose\n");
        Dealer.IsLose = true;
      }
      for (int i = 0; i < _users.Count; i++)
      {
        if (_users[i].Cards.Sum() == 21)
        {
          Console.WriteLine(_users[i].Name + " BLACK JACK");
          _users[i].Coins += _users[i].Bet*2;
        }
        if (_users[i].Cards.Sum() > 21 || _users[i].Cards.Sum() < Dealer.Cards.Sum() && Dealer.Cards.Sum() <= 21)
        {
          Console.WriteLine(_users[i].Name + " lose");
          _users[i].Coins -= _users[i].Bet;
        }
        if (_users[i].Cards.Sum() < 21 && _users[i].Cards.Sum() > Dealer.Cards.Sum() || Dealer.IsLose && _users[i].Cards.Sum()<21)
        {
          Console.WriteLine(_users[i].Name + " win");
          _users[i].Coins += _users[i].Bet;
        }
        if(_users[i].Cards.Sum()== Dealer.Cards.Sum()&& Dealer.Cards.Sum() <= 21)
        {
          Console.WriteLine(_users[i].Name + " win");
        }

      }
      Console.WriteLine("\nPlay again? P");
      if (Console.ReadKey().KeyChar == 'p')
      {
        Console.Clear();
        Start();
      }
    }


    public void GiveOneCard(User user)
    {
      Console.Clear();
      if (user.Cards.Sum() >= 21)
      {
        Console.WriteLine("!!!perebor!!!");
        return;
      }
      user.Cards.Add(_r.Next(2, 12));
    }
  }
}
